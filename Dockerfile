from ubuntu:18.04

RUN apt-get update && apt-get install -y \
    automake\
    autoconf\
    cmake \
    cppcheck \
    curl \
    git \
    g++ \
    gcc \
    libtool \
    locales \
    make \
    nano \
    wget \
    && rm -rf /var/lib/apt/listts/*

# Ubuntu /bin/sh points to dash, not bash. fix this.
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# We need to have an UTF8-capable locale for the yacto build.
RUN locale-gen en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN git clone git://github.com/cpputest/cpputest.git \
    && cd cpputest \
    && autoreconf . -i \
    && ./configure \
    && make

ENV CPPUTEST_HOME /cpputest

RUN mkdir /workspace